__author__ = 'mstijlaart'

from .feedburnercontroller import FeedburnerController
from lib.updater import Updater
from lib.feedstore import FeedStore
from .lib.feedproperties import FeedProperties

class Brick():

    def __init__(self, mapper, prefix, path):
        self.mapper = mapper
        self.prefix = prefix
        self.path = path
        self.name = self.prefix.split('/')[-1]
        self.feed_properties = FeedProperties(path + '/db')
        self.feed_store = FeedStore()
        self.updater = Updater(self.feed_store, feed_properties=self.feed_properties)
        self.setup_mapper()

    def setup_mapper(self):
        if self.prefix and self.mapper:
            controller = FeedburnerController(
                prefix=self.prefix,
                feed_store=self.feed_store,
                update_callback = self.update,
                feed_properties = self.feed_properties
            )
            self.mapper.connect(name='dash-brick-'+self.name, route=(self.prefix + '/'), controller=controller, action = 'index')
            self.mapper.connect(name='dash-brick-'+self.name, route=(self.prefix + '/settings'), controller=controller, action = 'settings')
            self.mapper.connect(name='dash-brick-'+self.name, route=(self.prefix + '/saveFeedName'), controller=controller, action = 'save_feed_name')

    def update(self):
        self.updater.execute()

    def get_javascript(self):
        js = "<script type=\"text/javascript\" src=\""+ self.prefix + "/brick.js\"></script>\n"
        js += "<script> var " + self.name + " = new feedburner_class('"+ self.name +"')</script>\n"
        js += "<script>" + self.name + ".render()</script>"
        return js

    def timeout(self):
        return 300

    def get_real_name(self):
        return 'dash-brick-sickbeard'