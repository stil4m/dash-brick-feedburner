function feedburner_class(name) {

    this.name = name;
    var elementID = this.name;
    var path = '/plugins/' + elementID;
    var settingsElementId = this.name + 'Settings'


    this.render = function() {
        dash.render(path + '/', elementID);
    };

    this.settings = function() {
        dash.modalSettings(
            path + '/settings',
            elementID,
            settingsElementId,
            this.render
        );
    };

    this.exitSettings = function () {
        dash.hideModalSettings(settingsElementId);
    };

    this.saveFeedName = function(element) {
        var buttonElement = $(element);
        buttonElement.button('loading');
        $.get(
            path + '/saveFeedName',
            {
                'feed_name': $(element).parent().find('#feed_name').val()
            },
            function() {
                buttonElement.button('reset');
            },
            'json'
        );
    }
};