__author__ = 'mstijlaart'

class FeedStore:

    def __init__(self):
        self.feeds = []

    def extend(self, feeds):
        self.feeds.extend(feeds)

    def clear(self):
        del self.feeds[:]

    def all_feeds(self):
        return self.feeds