__author__ = 'mstijlaart'

import shelve

class FeedProperties:

    def __init__(self, store_path):
        store_path += '/Store.shelf'
        self.shelve = shelve.open(filename=store_path)


    def get_feed_name(self):
        try:
            return self.shelve['feed_name']
        except:
            return ''

    def set_feed_name(self, new_name):
        self.shelve['feed_name'] = new_name
        self.save()

    def save(self):
        self.shelve.sync()