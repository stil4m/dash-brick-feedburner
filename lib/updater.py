__author__ = 'mstijlaart'

import json
import urllib2
from .feed import Feed
class Updater:

    def __init__(self, feed_store, feed_properties):
        self.feed_store = feed_store
        self.feed_properties = feed_properties


    def execute(self):
        print "UPDATE START"
        url = 'http://ajax.googleapis.com/ajax/services/feed/load?v=1.0&num=5&q=http://feeds.feedburner.com/' + self.feed_properties.get_feed_name()
        req = urllib2.Request(url)
        response = urllib2.urlopen(req)
        the_page = response.read()
        json_object =  json.loads(the_page)

        try:
            entries = json_object['responseData']['feed']['entries']

            new_feeds = []
            for entry in entries:
                new_feeds.append(Feed(entry))

            self.feed_store.clear()
            self.feed_store.extend(new_feeds)
            print "UPDATE DONE"
        except:
            self.feed_store.clear()
