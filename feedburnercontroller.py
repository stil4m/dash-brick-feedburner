__author__ = 'mstijlaart'

from app.controllers import BaseController
from jinja2 import Environment, FileSystemLoader

class FeedburnerController(BaseController):

    def __init__(self, prefix, feed_store, update_callback, feed_properties):
        self.feed_properties = feed_properties
        BaseController.__init__(self)
        self.feed_store = feed_store
        self.update_callback = update_callback
        self.plugin_name = prefix.split('/')[-1]
        self.env = Environment(loader=FileSystemLoader(prefix.lstrip('/') + '/templates'))


    def index(self):
        template = self.env.get_template('index.html')
        feeds = self.feed_store.all_feeds()
        return template.render(name=self.plugin_name, feeds=feeds)

    def settings(self):
        template = self.env.get_template('settings.html')
        return template.render(name=self.plugin_name, feed_name=self.feed_properties.get_feed_name())

    def save_feed_name(self, feed_name=''):
        print "NEW FEED= " + feed_name
        self.feed_properties.set_feed_name(feed_name)
        self.update_callback()
